ARG PARENT_IMAGE
FROM $PARENT_IMAGE

ARG PRESTODB_VERSION
ENV PRESTODB_VERSION=$PRESTODB_VERSION

LABEL maintainer="gitlab@therack.io"
LABEL description="PrestoDB ${PRESTODB_VERSION}"
LABEL prestodb_version="${PRESTODB_VERSION}"

RUN \
    apt update && \
    apt -qq -y install wget python python3 less && \
    apt -qq -y -o Dpkg::Options::="--force-confold" full-upgrade && \
    python --version && \
    python3 --version && \
    mkdir -p /opt && \
    mkdir -p /presto-data && \
    cd /opt && \
    # Get the server
    wget https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/prestodb-bin-arm64/$PRESTODB_VERSION/presto-server-$PRESTODB_VERSION.tar.gz -q --show-progress --progress=bar:force 2>&1 && \
    mkdir -p /presto-data && \
    tar -xzf /opt/presto-server-$PRESTODB_VERSION.tar.gz && \
    rm -rf /opt/presto-server-$PRESTODB_VERSION.tar.gz && \
    ln -s /opt/presto-server-${PRESTODB_VERSION} /opt/presto && \
    mkdir -p /opt/presto/etc && \
    ls -al /opt/presto && \
    cd / && \
    # Get the CLI
    wget https://s3.wasabisys.com/therackio-releases/therackio/big-data/binaries/prestodb-bin-arm64/$PRESTODB_VERSION/presto-cli-$PRESTODB_VERSION-executable.jar -q --show-progress --progress=bar:force 2>&1 && \
    mv presto-cli-${PRESTODB_VERSION}-executable.jar /usr/local/bin/presto && \
    chmod +x /usr/local/bin/presto

ENV PRESTO_HOME="/opt/presto"
ENV PATH="${PRESTO_HOME}/bin:${PATH}"

RUN \
    # Server Check
    launcher --help && \
    # Client Check
    presto --help

VOLUME ["/presto-data"]

CMD ["bash"]
